from setuptools import find_packages, setup

setup(
    name='src',
    packages=find_packages(),
    version='0.1.0',
    description='Vector search engines with Python: from embeddings to similarity search',
    author='IOMED',
    license='MIT',
)
