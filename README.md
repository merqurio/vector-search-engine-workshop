vector_search_engine_workshop
==============================

Vector search engines with Python: from embeddings to similarity search

## Description

Embeddings can be used to represent unstructured data such as images,movies,
text,.. as low-dimensional vectors in a way that similar items are close to
each other in a vector space. There are many existing mathematical techniques
for capturing the important structure of a high-dimensional data in a
low-dimensional space. We will explore some of them, create some examples and
use a vector search engine to solve a specific use case.


## Index

- What are Vector Embeddings?
- What is a Vector Database?
- What is Vector Similarity Search?
- So what do I do with my vector embedding?
- Efficient Filtering of Vectors
- Use cases: text, image, video, audio, graph and relational data
- Let's build a search engine: Semantic textual search with vector embeddings
    1. Get the data
    1. Create the embeddings
    1. Upload the embeddings
    1. Query the data

## Requirements

- Python 3.8 or newer
- Jupyter notebook
- docker >= 17.12.0+
- docker-compose

## Quick Start
- Clone or download this repository
- Go inside the directory
- Run this command `docker-compose up -d`


## Environments
This Compose file contains the following environment variables:

- `POSTGRES_USER` the default value is --postgres**
- `POSTGRES_PASSWORD` the default value is **changeme**
- `PGADMIN_PORT` the default value is **5050**
- `PGADMIN_DEFAULT_EMAIL` the default value is **pgadmin4@pgadmin.org**
- `PGADMIN_DEFAULT_PASSWORD` the default value is **admin**

## Access to postgres:
- `localhost:5432`
- **Username:** postgres (as a default)
- **Password:** changeme (as a default)

## Access to PgAdmin:
- **URL:** `http://localhost:5050`
- **Username:** pgadmin4@pgadmin.org (as a default)
- **Password:** admin (as a default)

## Add a new server in PgAdmin:
- **Host name/address** `postgres`
- **Port** `5432`
- **Username** as `POSTGRES_USER`, by default: `postgres`
- **Password** as `POSTGRES_PASSWORD`, by default `changeme`


## Project Organization


    ├── LICENSE
    ├── Makefile           <- Makefile with commands like `make data` or `make train`
    ├── README.md          <- The top-level README for developers using this project.
    ├── data
    │   ├── external       <- Data from third party sources.
    │   ├── interim        <- Intermediate data that has been transformed.
    │   ├── processed      <- The final, canonical data sets for modeling.
    │   └── raw            <- The original, immutable data dump.
    │
    ├── docs               <- A default Sphinx project; see sphinx-doc.org for details
    │
    ├── models             <- Trained and serialized models, model predictions, or model summaries
    │
    ├── notebooks          <- Jupyter notebooks. Naming convention is a number (for ordering),
    │                         the creator's initials, and a short `-` delimited description, e.g.
    │                         `1.0-jqp-initial-data-exploration`.
    │
    ├── references         <- Data dictionaries, manuals, and all other explanatory materials.
    │
    ├── reports            <- Generated analysis as HTML, PDF, LaTeX, etc.
    │   └── figures        <- Generated graphics and figures to be used in reporting
    │
    ├── requirements.txt   <- The requirements file for reproducing the analysis environment, e.g.
    │                         generated with `pip freeze > requirements.txt`
    │
    ├── setup.py           <- makes project pip installable (pip install -e .) so src can be imported
    ├── src                <- Source code for use in this project.
    │   ├── __init__.py    <- Makes src a Python module
    │   │
    │   ├── data           <- Scripts to download or generate data
    │   │   └── make_dataset.py
    │   │
    │   ├── features       <- Scripts to turn raw data into features for modeling
    │   │   └── build_features.py
    │   │
    │   ├── models         <- Scripts to train models and then use trained models to make
    │   │   │                 predictions
    │   │   ├── predict_model.py
    │   │   └── train_model.py
    │   │
    │   └── visualization  <- Scripts to create exploratory and results oriented visualizations
    │       └── visualize.py
    │
    └── tox.ini            <- tox file with settings for running tox; see tox.readthedocs.io


--------

<p><small>Project based on the <a target="_blank" href="https://drivendata.github.io/cookiecutter-data-science/">cookiecutter data science project template</a>. #cookiecutterdatascience</small></p>
